<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//HOME FOR THE WEB NOT YET CREATED
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//------------------CUSTOMER--------------------
//Logged in Customers cannot access or send requests these pages
Route::group(['middleware' => 'customer_guest'], function(){
    Route::get('cust_register', 'CustomerAuth\RegisterController@showRegistrationForm');
    Route::post('cust_register', 'CustomerAuth\RegisterController@register');
    Route::get('cust_login', 'CustomerAuth\LoginController@showLoginForm');
    Route::post('cust_login', 'CustomerAuth\LoginController@login');
    
});

//Only logged in Customers can access or send requests to these pages
Route::group(['middleware' => 'customer_auth'], function(){
    Route::post('cust_logout', 'CustomerAuth\LoginController@logout');
    Route::get('/customer_home', function(){
    return view('Customer.home');
    });
    Route::get('/customer_home/edit', 'Customer\crud@edit');
    Route::patch('/customer_home/edit', 'Customer\crud@update');
    Route::post('/customer_home/order', 'Customer\crud@store');
    Route::get('/customer_home/list', 'Customer\crud@show');
    Route::delete('/customer_home/list/{id}', 'Customer\crud@destroy');
});


//------------------TALENT------------------
//Logged in Talents cannot access or send request these pages
Route::group(['middleware' => 'talent_guest'], function(){
    Route::get('talent_register', 'TalentAuth\RegisterController@showRegistrationForm');
    Route::post('talent_register', 'TalentAuth\RegisterController@register');
    Route::get('talent_login', 'TalentAuth\LoginController@showLoginForm');
    Route::post('talent_login', 'TalentAuth\LoginController@login');
});
//Only logged in Talents can access or send requests to these pages
Route::group(['middleware' => 'talent_auth'], function(){
    Route::post('talent_logout', 'TalentAuth\LoginController@logout');
    Route::group(['middleware' => 'check_order'], function(){
        Route::get('/talent_home', 'Talent\crud@index');
        Route::get('/talent_home/edit', 'Talent\crud@edit'); 
    });
    Route::get('/talent_home/order', 'Talent\crud@show')->name('order_list');
    Route::patch('/talent_home/edit', 'Talent\crud@update');
    Route::patch('/talent_home/order/{id}', 'Talent\crud@store');
});