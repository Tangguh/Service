<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    //
    protected $table = 'orders';
    protected $fillable = [
      'id_customer','id_talent', 'title', 'date_order', 'description', 'destination', 'updated_at'
    ];
    protected $guarded = [
        'id', 'created_at'
    ];
}
