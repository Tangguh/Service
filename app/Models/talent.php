<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class talent extends Authenticatable
{
    //
    protected $table = 'talents';
    protected $fillable = [
      'name', 'username', 'password', 'updated_at'
    ];
    protected $hidden = [
        'password', 'remember_token', 'created_at'
    ];
}
