<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class customer extends Authenticatable
{
    //
    protected $table = 'customers';
    protected $fillable = [
      'name', 'address', 'username', 'password','updated_at'
    ];
    protected $hidden = [
        'password', 'remember_token', 'created_at'
    ];
}
