<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Class needed for login and Logout logic
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    //Redirect after login
    protected $redirectTo = '/customer_home';
    //Trait
    use AuthenticatesUsers;
    //Change Default to Username
    public function username()
    {
        return 'username'; //add username field here
    }
    //Custom guard for customer
    protected function guard()
    {
      return Auth::guard('web_customer');
    }
    //Shows Customer login form
    public function showLoginForm()
    {
        return view('Customer.auth.login');
    }
}
