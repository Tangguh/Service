<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Use Database 'customer'
use App\Models\customer;
//Use Authentication
use Auth;
use Hash;

class RegisterController extends Controller
{
    //----------Register------------
    
    protected $redirectPath = 'customer_home';
    
    //Show Registration Form
    public function showRegistrationForm(){
        return view('Customer.auth.register');
    }
    public function register(Request $request)
    {

       //Validates data
       //$this->validator($request->all())->validate();
       $this->validate($request,[
            'name'=> 'required|min:5|max:100',
            'address' => 'required|min:5|max:314',
            'username' => 'required|min:5|max:100|unique:customers',
            'password' => 'required|min:5|max:255'
        ]);

       //Create customer
        $customer = $this->create($request->all());

       //Authenticates customer
        $this->guard()->login($customer);
        
       //Redirects customer
        return redirect($this->redirectPath);
    }
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:100',
            'address' => 'required|max:314',
            'username' => 'required|max:100',
            'password' => 'required|max:255',
        ]);
    }*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function create(array $data)
    {
        //
        return customer::create([
            'name' => $data['name'],
            'address' => $data['address'],
            'username' => $data['username'],
            //'password' => bcrypt($data['password']),
            'password' => Hash::make($data['password']),
        ]);
    }
    protected function guard()
   {
       return Auth::guard('web_customer');
   }
}
