<?php

namespace App\Http\Controllers\Talent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Use Database 'talent'
use App\Models\talent;
//Use Database 'order'
use App\Models\order;
//Use Databse 'customer'
use App\Models\customer;
use Auth;

class crud extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $orders = order::where('status', '=', 'Ask')->get();
        $id = order::select('id_customer')->where('status', '=', 'Ask')->get();
        $customers = customer::whereIn('id', $id)->get();
        return view('Talent.home')
            ->with(compact('orders'))
            ->with(compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        $order = order::findOrFail($id);
        $order->id_talent = $request->id_talent;
        $order->status = $request->status;
        $order->updated_at=$request->updated_at;
        $order->save();
        if($request->status === 'Take'){
        return redirect()->route('order_list')->with('alert-success', 'Pesanan telah diambil, silahkan dijalankan!');    
        }
        return redirect()->route('order_list')->with('alert-success', 'Status telah dirubah, jangan sampai terlambat!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //Tambahkan error page
        $order = order::where('id_talent', Auth::guard('web_talent')->user()->id)->first();
        if($order === null){
            return redirect('/talent_home');
        }
        $orders = order::where('id_talent', Auth::guard('web_talent')->user()->id)->get();
        $customers = customer::where('id', $order->id_customer)->get();
        return view('talent.list')
            ->with(compact('orders'))
            ->with(compact('customers'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        $talent = talent::findOrFail(Auth::guard('web_talent')->user()->id);
        return view('talent.profile',compact('talent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $talent = talent::findOrFail(Auth::guard('web_talent')->user()->id);
        $talent->name = $request->name;
        $talent->username=$request->username;
        $talent->updated_at=$request->updated_at;
        $talent->save();
        //return redirect()->route('talent.edit', $id)->with('alert-success', 'Data berhasil diubah.');
        return back()->withInput()->with('alert-success', 'Data berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
