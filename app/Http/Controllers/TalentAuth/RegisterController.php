<?php

namespace App\Http\Controllers\TalentAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Use Database 'talent'
use App\Models\talent;
//Use Authentication
use Auth;
use Hash;

class RegisterController extends Controller
{
    //----------Register------------
    
    protected $redirectPath = 'talent_home';
    
    //Show Registration Form
    public function showRegistrationForm(){
        return view('Talent.auth.register');
    }
    public function register(Request $request)
    {

       //Validates data
       //$this->validator($request->all())->validate();
       $this->validate($request,[
            'name'=> 'required|min:5|max:100',
            'username' => 'required|min:5|max:100|unique:talents',
            'password' => 'required|min:5|max:255'
        ]);

       //Create Talent
        $talent = $this->create($request->all());

       //Authenticates Talent
        $this->guard()->login($talent);
        
       //Redirects Talent
        return redirect($this->redirectPath);
    }
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:100',
            'address' => 'required|max:314',
            'username' => 'required|max:100',
            'password' => 'required|max:255',
        ]);
    }*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function create(array $data)
    {
        //
        return talent::create([
            'name' => $data['name'],
            'username' => $data['username'],
            //'password' => bcrypt($data['password']),
            'password' => Hash::make($data['password']),
        ]);
    }
    protected function guard()
   {
       return Auth::guard('web_talent');
   }
}
