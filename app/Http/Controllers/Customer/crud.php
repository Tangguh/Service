<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Use Database 'customer'
use App\Models\customer;
//Use Database 'order'
use App\Models\order;
use Gate;
use Auth;


class crud extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Customer.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $orders = new order();
        $orders->id_customer = $request->id_customer;
        $orders->title = $request->title;
        $orders->date_order = $request->date_order;
        $orders->description = $request->description;
        $orders->destination = $request->destination;
        $orders->save();
        return back()->with('alert-success', 'Berhasil membuat pesanan! bisa dicek di Pesanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $orders = order::where('id_customer', Auth::guard('web_customer')->user()->id)->get();;
        return view('Customer.list',compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        $customer = customer::findOrFail(Auth::guard('web_customer')->user()->id);
        return view('customer.profile',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $customer = customer::findOrFail(Auth::guard('web_customer')->user()->id);
        $customer->name = $request->name;
        $customer->address=$request->address;
        $customer->username=$request->username;
        $customer->updated_at=$request->updated_at;
        $customer->save();
        //return redirect()->route('customer.edit', $id)->with('alert-success', 'Data berhasil diubah.');
        return back()->withInput()->with('alert-success', 'Data berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $orders = order::findOrFail($id);
        $orders->delete();
        return back()->with('alert-success', 'Pesanan sudah terlaksanakan.');
    }
}
