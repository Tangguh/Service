<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\order;

class TalentHaveOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $thereIsOrder = order::where('id_talent', Auth::guard('web_talent')->user()->id)->pluck('id_talent')->first();
       if ( $thereIsOrder === null) {
           return $next($request);
           //return redirect()->route('order_list');
       }
        return redirect('/talent_home/order');
        //return $next($request);
    }
}
