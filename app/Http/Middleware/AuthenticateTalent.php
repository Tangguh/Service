<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateTalent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If request doesn't come from logged in Talent
        //It will be redirected to the login form page
        if(! Auth::guard('web_talent')->check()){
            return redirect('/talent_login');
        }
        return $next($request);
    }
}
