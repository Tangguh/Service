<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
class AuthenticateCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If request doesn't come from logged in Customer
        //It will be redirected to the login form page
        if(! Auth::guard('web_customer')->check()){
            return redirect('/cust_login');
        }
        return $next($request);
    }
}
