<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
class RedirectIfCustomerAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If request does come from logged in Customer
        //It will be redirected to the customer home page
        if( Auth::guard('web_customer')->check()){
            return redirect('/customer_home');
        }
        return $next($request);
    }
}
