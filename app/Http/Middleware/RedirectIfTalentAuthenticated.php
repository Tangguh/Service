<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class RedirectIfTalentAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If request does come from logged in Talent
        //It will be redirected to the customer home page
        if( Auth::guard('web_talent')->check()){
            return redirect('/talent_home');
        }
        return $next($request);
    }
}
