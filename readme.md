# Service App
This is just an example of rocky start to use the powerful framework called <a href="https://laravel.com/">LARAVEL</a>. It took me a month to make this project, the great things is i don't even know what laravel is before i start with this project, so basically i just learn and make at the same time it's kinda hard tho XD

## About Service App
A basic web-application for service an order, there are two users can use this app, which are `Customers` & `Talents`. There are also 3 table (Customer, Talent, Order). The customer can have many order, and the talent only serve order one by one at the time.

## Customer
This is how to use costumer, the flow is simple.
- Customer choose whether Login or Register
- After that, the customer will redirected to the home page
- In home page, they can order any service that available
- Customer can click the "Pesanan" and see what they've been ordered, there is a table of it
- Customer can finish the order by clicking "Sudahi" button, when ever they want to finish
- Otherwise, customer can log out and go back to the welcome page

## Talent
This is more complex controler, but also simple to use
- Talent choose Login or Register
- They will be redireted to the home page
- And the list of orders, shows up. Talent can take which one they want to serve
- After talent take an order, they can update status of the order by clicking the "Jalankan" button
- Otherwise, talent can log out and go back to the welcome page

### Footer copyright © 2017 RVNDEV