@extends('Talent.layouts')

@section('title')
    List Pesanan
@endsection

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">Pesanan</div>
                <div class="panel-body">
                   @if(Session::has('alert-success'))
	                   <div class="alert alert-success">
		                    {{ Session::get('alert-success') }}
	                   </div>
                   @endif
                   <h3>Selesaikan satu orderan dulu</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Title</td>
                                <td>Order date</td>
                                <td>Customer</td>
                                <td>Description</td>
                                <td>Destination</td>
                                <td>Status</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; ?>
                           @foreach($orders as $order)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$order->title}}</td>
                                <td>{{$order->date_order}}</td>
                                @foreach($customers as $customer)
                                <td>{{$customer->name}}</td>
                                @endforeach
                                <td>{{$order->description}}</td>
                                <td>{{$order->destination}}</td>
                                <td>{{$order->status}}</td>
                                <!--<td><a href="#action">In Progress</a></td>-->
                                <td>
                                   @if($order->status === 'Take')
                                   <form method="POST" action="{{url('/talent_home/order', $order->id)}}" accept-charset="UTF-8">
                                        <input type="hidden" name="_method" value="PATCH">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id_talent" value="{{ Auth::guard('web_talent')->user()->id }}">
                                        <input type="hidden" name="status" value="In Progress">
                                        <input type="hidden" name="updated_at" value='<?php echo date("Y-m-d G:H:s"); ?>'>
                                        <input type="submit" value="Jalankan" class="btn btn-info" onclick="return confirm('Waktunya berangkat??? Sekarang');">
                                    </form>
                                   @else
                                   Telah diambil
                                   @endif   
                                </td>    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection