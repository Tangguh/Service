@extends('Talent.layouts')

@section('title')
    Edit Profile
@endsection

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">Profil Talent</div>
                <div class="panel-body">
                   @if(Session::has('alert-success'))
	                   <div class="alert alert-success">
		                    {{ Session::get('alert-success') }}
	                   </div>
                   @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{url('/talent_home/edit')}}">
                        <input type="hidden" name="_method" value="PATCH">
                        {{ csrf_field() }}
                        
<!--Name-->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$talent->name}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<!--Username-->
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" value="{{$talent->username}}" name="username" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="updated_at" value='<?php echo date("Y-m-d H:i:s"); ?>'>
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-info">
                                    Update Talent
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection