<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Service</title>
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background: linear-gradient(to right, #26b4e8 , #72f2ce);
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 80px;
                
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 20px;
            }
            
            .lightSpeedIn{
                animation-duration: 1s;
            }
            .bounceInDown {
                animation-duration: 1s;
            }
            .rotateInDownRight {
                animation-delay: 1.5s;
            }
            .rotateInDownLeft {
                animation-delay: 2s;
            }
            .fadeInLeft {
                animation-delay: 2.5s;
            }
            .zoomInDown {
                animation-delay: 3s;
            }
            .zoomInUp {
                animation-delay: 3.5s;
            }
            .fadeInRight {
                animation-delay: 4s;
            }
            .jackInTheBox {
                animation-delay: 4.5s;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links animated bounceInDown">
                    @if (Auth::guard('web_customer')->check())
                        <a href="{{ url('/customer_home') }}">Home</a>
                    @elseif(Auth::guard('web_talent')->check())
                        <a href="{{ url('/talent_home') }}">Home</a>
                    @else    
                        <a href="{{ url('/cust_login') }}">Customer</a>
                        <a href="{{ url('/talent_login') }}">Talent</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md animated lightSpeedIn">
                    Service App
                </div>
                    <h3 class="animated rotateInDownRight">Service</h3>
                    <p class="animated rotateInDownLeft"><i><b>verb </b>ser·vice</i></p>
                    <hr class="animated fadeInLeft">
                    <h4 class="animated zoomInDown">Definition of service</h4>
                    <p class="animated zoomInUp"><i>serviced; servicing</i>
                    :  to perform <u><b>services</b></u> for: such as.</p>
                    <hr class="animated fadeInRight">
              <h2 class="animated jackInTheBox"><q>We do best so you can joy</q></h2>
            </div>
        </div>
    </body>
</html>
