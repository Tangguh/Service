@extends('Customer.layouts')

@section('title')
    List Pesanan
@endsection

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">Pesanan</div>
                <div class="panel-body">
                   @if(Session::has('alert-success'))
	                   <div class="alert alert-success">
		                    {{ Session::get('alert-success') }}
	                   </div>
                   @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Title</td>
                                <td>Order date</td>
                                <td>Description</td>
                                <td>Destination</td>
                                <td>Status</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; ?>
                           @foreach($orders as $order)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$order->title}}</td>
                                <td>{{$order->date_order}}</td>
                                <td>{{$order->description}}</td>
                                <td>{{$order->destination}}</td>
                                <td>{{$order->status}}</td>
                                <td>
                                    <form method="POST" action="{{ url('/customer_home/list', $order->id)}}" accept-charset="UTF-8">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" value="Sudahi" class="btn btn-info" onclick="return confirm('Sudah kah anda puas???');">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection