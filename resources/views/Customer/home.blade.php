@extends('Customer.layouts')

@section('title')
    Selamat Datang
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   @if(Session::has('alert-success'))
	                   <div class="alert alert-success">
		                    {{ Session::get('alert-success') }}
	                   </div>
                   @endif
                    <!--Greetings.. {{ Auth::guard('web_customer')->user()->name }}-->
                    <h3>Hari ini mau pesan pelayanan apa???</h3>
                    <hr>
                     
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/customer_home/order') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_customer" id="id_customer" value="{{ Auth::guard('web_customer')->user()->id }}">
<!--Title-->
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-4">
                                <!--<input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>-->
                                <select name="title" id="title" class="form-control">
                                    <option value="">Pilih Satu</option>
                                    <option value="Pijat">Pijat</option>
                                    <option value="Bersih-bersih Rumah">Bersih-bersih Rumah</option>
                                    <option value="Jaga Rumah">Jaga Rumah</option>
                                </select>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<!--Date order-->
                        <div class="form-group{{ $errors->has('date_order') ? ' has-error' : '' }}">
                            <label for="date_order" class="col-md-4 control-label">Order on</label>

                            <div class="col-md-4">
                                <input id="date_order" type="date" class="form-control" name="date_order" value="{{ old('title') }}" min="<?php echo date('Y-m-d'); ?>" required autofocus>
                                @if ($errors->has('date_order'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date_order') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                        
<!--Description-->
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-4">
                                <textarea id="description" class="form-control" name="description" required>{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<!--Destination-->
                       <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                            <label for="destination" class="col-md-4 control-label">Alamat Tujuan</label>

                            <div class="col-md-4">
                                <textarea maxlength="200" id="destination" class="form-control" name="destination" required>{{ old('destination') }}</textarea>
                                @if ($errors->has('destination'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destination') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Buat Pesanan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection